import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import {
  Tabs,
  Tab,
  Body,
  Content,
  Text,
  Button,
  Icon,
  Container,
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { RNCamera } from 'react-native-camera';
import QRCodeScanner from 'react-native-qrcode-scanner';
import ReceiveForm from './../coomponents/ReceiveForm';
import styles from 'IfulifeManage/src/constants/styles';

export default class MenuReceive extends Component<Props>{
  camera: RNCamera | null;
  qrcode: QRCodeScanner | null;

  constructor(props) {
    super(props);

    this.state = {
      nowTab : 0,
      scanSuccess: false,
      scanCode: ''
    }

    this.camera = null;
    this.qrcode = null;

    this.switchCamera = this.switchCamera.bind(this);
  }


  switchCamera(page) {
    this.setState({nowTab: page});
    this.refs.cameraTab.goToPage(page);
  }


  reActiveScan() {
    this.scanner.reactivate()
  }


  onReaqCodeSuccess = (e) => {
    this.setState({
      scanSuccess: true,
      scanCode: e.data
    })
  }

  render() {
    const { isVisible } = this.props;

    return (
      <View style={styles.modalView}>
        <View style={styles.modalViewInside}>

          <Button iconLeft light small style={styles.modalCloseBtn}
            onPress={() => { this.props.navigation.goBack(); }}
          >
            <Icon type="FontAwesome" name="window-close" style={styles.modalCloseIcon} />
            <Text style={styles.modalCloseText}>關閉</Text>
          </Button>

          <Grid style={styles.modalContent}>
            <Col>
              <ReceiveForm
                switchCamera={this.switchCamera}
                scanCode={this.state.scanCode}
                reActiveScan={this.reActiveScan}
              />
            </Col>
            <Col style={{ marginBottom:40 }}>
              <Tabs ref="cameraTab" initialPage={this.state.defaultTab}>
                <Tab heading="包裹拍照">
                  <Container>
                    <Body>
                      <RNCamera
                        ref={ref => {
                          this.camera = ref;
                        }}
                        type={RNCamera.Constants.Type.back}
                        style={styles.cameraBlock}
                        flashMode={RNCamera.Constants.FlashMode.off}
                      />
                      <View style={{ width: '100%', height: 50, position: "absolute", bottom: 0 }}>
                        <Button block iconLeft success style={[styles.OptButton, { width: 200, height: 50, alignSelf: 'center', }]} onPress={() => this.takePicture()}>
                          <Icon type="Entypo" name="camera" style={styles.OptButtonIcon} />
                          <Text style={styles.OptButtonLabel}>點我拍照</Text>
                        </Button>
                      </View>
                    </Body>
                    {/* 設定數量上限2 */}
                    {/* 拍照後照片檔只能刪除，再重拍 */}

                  </Container>
                </Tab>
                <Tab heading="物流單掃碼">
                  <Container>
                    <Grid>
                    <Row size={0.7} style={{ flexDirection: 'column' }}>
                      <QRCodeScanner
                        ref={(node) => { this.qrcode = node }}
                        reactivate={false}
                        showMarker={true}
                        containerStyle={[styles.cameraBlock, { margin: 20, }]}
                        cameraStyle={{ width: 360, height: 360, alignSelf: 'center' }}
                        cameraType="back"
                        onRead={this.onReadCodeSuccess}
                      />
                    </Row>
                    {this.state.scanSuccess &&
                    <Row size={0.3} style={{flexDirection:'column', alignItems: 'center',}}>
                      <Text>不連續辨識，完成辨識後掃碼元件鎖死，除非左側點選修改。</Text>
                      <Button block success style={styles.OptButton}>
                        <Text style={styles.OptButtonLabel}>重新掃描</Text>
                      </Button>
                    </Row>
                    }
                    </Grid>
                  </Container>
                </Tab>
              </Tabs>
            </Col>
          </Grid>

        </View>
      </View>
    )
  }
}
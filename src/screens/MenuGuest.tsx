import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
} from 'react-native';
import {
  Tabs,
  Tab,
  Body,
  Right,
  H2,
  Text,
  Button,
  Icon,
} from 'native-base';
import { RNCamera } from 'react-native-camera';
import { SketchCanvas } from '@gigasz/react-native-sketch-canvas';
import { Grid, Col, Row } from 'react-native-easy-grid';
import GuestForm from './../coomponents/GuestForm';
import GuestCard from './../coomponents/GuestCard';
import styles from 'IfulifeManage/src/constants/styles';

/**
 * 1. 塗鴉簽名後才會出現管理員確認按鈕
 * 2.
 */
export default class MenuGuest extends Component<Props>{
  camera: RNCamera | null;

  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {

    return (
      <View style={styles.modalView}>
        <View style={styles.modalViewInside}>

          <Button iconLeft light small style={styles.modalCloseBtn}
            onPress={() => { this.props.navigation.goBack(); }}
          >
            <Icon type="FontAwesome" name="window-close" style={styles.modalCloseIcon} />
            <Text style={styles.modalCloseText}>關閉</Text>
          </Button>

          <View style={styles.modalContent}>

            <Tabs>
              <Tab heading="簽入社區">
              <Grid>
                <Col>
                  <GuestForm />
                </Col>
                <Col>
                  <Row size={0.6} style={[{flexDirection: 'column',}]}>
                    <RNCamera
                      ref={ref => {
                        this.camera = ref;
                      }}
                      type={RNCamera.Constants.Type.back}
                      style={[styles.guestCameraBlock]}
                      flashMode={RNCamera.Constants.FlashMode.off}
                    />
                    <View style={{ width: '100%', height: 50, position: "absolute", bottom: 0}}>
                    <Button block iconLeft success style={[styles.OptButton, { width: 200, height: 50, alignSelf: 'center',}]} onPress={() => this.takePicture()}>
                      <Icon type="Entypo" name="camera" style={styles.OptButtonIcon} />
                      <Text style={styles.OptButtonLabel}>點我拍照</Text>
                    </Button>
                    </View>
                  </Row>
                  <Row size={0.4} style={styles.container}>
                    <SketchCanvas
                      style={{ flex: 1, backgroundColor: '#CECECE' }}
                      strokeColor={'red'}
                      strokeWidth={7}
                    />
                  </Row>
                </Col>
              </Grid>
              </Tab>
              <Tab heading="離開社區">
              <Grid>
                <Col>
                  <View style={{ flexDirection: 'row', marginVertical: 10, }}>
                    <Body>
                      <H2>尚未離開訪客</H2>
                    </Body>
                    <Right>
                      <Button block success small
                        onPress={() => {
                        }}
                      >
                        <Text>重新整理</Text>
                      </Button>
                    </Right>
                  </View>
                  <GuestCard />
                </Col>
                <Col>
                  <View style={{ flexDirection: 'row', marginVertical: 10, }}>
                    <Body>
                    <H2>今日到訪訪客</H2>
                    </Body>
                    <Right>
                    <Button block success small
                      style={{ marginLeft: 10, }}
                      onPress={() => {
                        // Linking
                        alert('引導開啟網頁版檢視！');
                      }}
                    >
                      <Text>查看訪客紀錄</Text>
                    </Button>
                    </Right>
                  </View>
                  <GuestCard />
                </Col>
              </Grid>
              </Tab>
            </Tabs>

          </View>

        </View>
      </View>
    )
  }
}
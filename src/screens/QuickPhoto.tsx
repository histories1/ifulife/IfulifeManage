import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  H2,
  Text,
  Button,
  Icon,
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import styles from 'IfulifeManage/src/constants/styles';
import { RNCamera } from 'react-native-camera';
import ResponsiveImage from 'react-native-responsive-image';
import { Dialog } from 'react-native-simple-dialogs';

/**
 * todo :
 * 1. 配置顯示相機區塊
 * 每拍到一張相片儲存到裝置相簿內，下方產生一筆縮圖紀錄(可點選多筆顯示下一步)
 * 2. 確認關閉後再開啟仍可顯示相機功能
 */

export default class QuickPhoto extends Component<Props>{
  camera: RNCamera | null;
  _photoicon: Array<string>;

  constructor(props) {
    super(props);

    this.state = {
      FlashMode : false,
      hasPhotos: false,
      takePhotos: [],
      selectedPhotos: [],
      dialogVisible : false
    }

    this.camera = null;
    this._photoicon = [];

    this._renderDialog = this._renderDialog.bind(this);
    this.takePicture = this.takePicture.bind(this);
    this.selectPhoto = this.selectPhoto.bind(this);

    this.allSelected = this.allSelected.bind(this);
    this.allStore = this.allStore.bind(this);
    this.toGuest = this.toGuest.bind(this);
    this.toReceive = this.toReceive.bind(this);
  }


  _renderDialog() {
    return (
      <Dialog
        visible={this.state.dialogVisible}
        title="Custom Dialog"
        onTouchOutside={() => this.setState({ dialogVisible: false })} >
        <View>
          <Text>顯示內容</Text>
        </View>
      </Dialog>
    )
  }

  // 處理拍照暫存動作
  async takePicture() {
    try {
      if( this.camera ) {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync(options);
        console.log('目前拍照: ', data);
        // 配置識別ID
        let tmp = data.uri.split("/");
        // console.log('識別: ', tmp,'索引: ',tmp[tmp.length - 1]);
        const nowphoto = {
          id: tmp[tmp.length - 1].replace('.jpg',''),
          uri: data.uri,
          height: data.height*0.25,
          width: data.width*0.25,
        }
        this.setState({
          takePhotos: [...this.state.takePhotos, nowphoto ]
        });
        console.log('照片結構: ', this.state.takePhotos);
        if (data.uri && !this.state.hasPhotos){
          this.setState({ hasPhotos: true });
        }
      }
    } catch (error) {
      console.warn('拍照暫存照片發生錯誤:', error.message);
    }
  }


  // 處理
  selectPhoto(photo) {
    console.log('Pass photo: ', photo);
    // console.log('Icons :', this._photoicon);
  }


  // 處理將瀑布流照片全部選擇
  async allSelected() {

  }


  // 處理暫存的相片全部儲存
  async allStore() {

  }

  // 將照片
  async toGuest() {
    this.props.navigation.navigate('Guest', {
      'photos' : this.state.selectedPhotos
    });
  }


  async toReceive() {
    this.props.navigation.navigate('Receive', {
      'photos': this.state.selectedPhotos
    });
  }


  render() {

    return (
      <View style={styles.modalView}>
        <View style={styles.modalViewInside}>

          <Button iconLeft light small style={styles.modalCloseBtn}
            onPress={() => { this.props.navigation.goBack(); }}
          >
            <Icon type="FontAwesome" name="window-close" style={styles.modalCloseIcon} />
            <Text style={styles.modalCloseText}>關閉</Text>
          </Button>

          {this._renderDialog()}

          <Grid style={styles.modalContent}>
            <Col>
              <Row size={0.7} style={styles.cameraBlock}>
                <RNCamera
                  ref={ref => {
                    this.camera = ref;
                  }}
                  type={RNCamera.Constants.Type.back}
                  style={styles.cameraBlock}
                  flashMode={(this.state.FlashMode) ? RNCamera.Constants.FlashMode.on : RNCamera.Constants.FlashMode.off}
                />
              </Row>
              <Row size={0.3} style={styles.cameraOptBlock}>
                <Col size={0.3}>
                  <Button rounded iconLeft style={styles.OptButtonSM} info={(this.state.FlashMode) ? false : true} onPress={() => this.setState({ FlashMode: !this.state.FlashMode })}>
                    <Icon type="MaterialIcons" name={(this.state.FlashMode) ? "flash-on" : "flash-off"} style={styles.OptButtonSMIcon} />
                    <Text style={styles.OptButtonSMLabel}>閃光燈</Text>
                  </Button>
                  <Button rounded iconLeft style={styles.OptButtonSM} info onPress={() => this.setState({ takePhotos: [], hasPhotos: false })}>
                    <Icon type="MaterialIcons" name="zoom-in" style={styles.OptButtonSMIcon} />
                    <Text style={styles.OptButtonSMLabel}>拉近</Text>
                  </Button>
                  <Button rounded iconLeft style={styles.OptButtonSM} info onPress={() => this.setState({ takePhotos: [], hasPhotos: false })}>
                    <Icon type="MaterialIcons" name="zoom-out" style={styles.OptButtonSMIcon} />
                    <Text style={styles.OptButtonSMLabel}>拉遠</Text>
                  </Button>
                </Col>
                <Col size={0.7}>
                  <Button block iconLeft success style={styles.OptButton} onPress={() => this.takePicture()}>
                    <Icon type="Entypo" name="camera" style={styles.OptButtonIcon} />
                    <Text style={styles.OptButtonLabel}>點我拍照</Text>
                  </Button>
                </Col>
              </Row>
            </Col>


            <Col>
              {
                (this.state.hasPhotos) ?
              (
                <Grid>
                  {
                    ( this.state.selectedPhotos.length == 0 ) ?
                    (
                      <Row size={0.2} style={{ flexDirection: 'column', }}>
                      <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <Text>請點選下方相片：(請注意，若直接點選離開，畫面顯示的相片將不會儲存。)</Text>
                      </View>
                      </Row>
                    ) :
                            (
                      <Row size={0.2} style={{ flexDirection: 'column', }}>
                      <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <Button block info small style={{marginRight:10}} onPress={() => this.allSelected()}>
                          <Text style={{ color: 'white' }}>全部選取</Text>
                        </Button>
                        <Button block danger small style={{marginRight:10}} onPress={() => this.selectedClear()}>
                          <Text style={{ color: 'white' }}>清除選擇</Text>
                        </Button>
                        <Button block primary small style={{marginRight:10}} onPress={() => this.selectedStore()}>
                          <Text style={{ color: 'white' }}>儲存到照片</Text>
                        </Button>
                      </View>
                      <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <Button block success onPress={() => this.toGuest()}>
                          <Text style={{ color: 'white' }}>作為訪客相片</Text>
                        </Button>
                        <Button block success onPress={() => this.toReceive()}>
                          <Text style={{ color: 'white' }}>建立包裹資訊</Text>
                        </Button>
                      </View>
                      </Row>
                    )
                  }

                  <Row size={0.8}>
                    <Content style={{marginBottom: 30,}}>
                      <View style={{flexDirection: 'row', flexWrap: 'wrap', backgroundColor: 'pink'}}>
                        {this.state.takePhotos.map(function(photo, key){
                          return (
                            <TouchableOpacity key={key} onPress={() => selectPhoto(photo)}>
                              <ResponsiveImage
                                initWidth={200}
                                initHeight={150}
                                source={photo}
                                style={{margin:10}}
                                resizeMode="stretch"
                              />
                              <Icon  type="Ionicons" name="ios-checkmark-circle-outline" size={20} style={styles.rwdIcon} />
                            </TouchableOpacity>
                          )
                        })}
                      </View>
                    </Content>
                  </Row>
                </Grid>
              ) :
              (
                <Content padder>
                  <H2>將後置鏡頭對準物品再點選拍照，相機會自動對焦並暫存相片於右側列表。</H2>
                  <Text/>
                  <H2>選擇一張以上相片後，可對項目進行「訪客登記」「包裹收件」或直接儲存於裝置「照片」程式內操作。</H2>
                </Content>
              )
              }
            </Col>
          </Grid>

        </View>
      </View>
    )
  }
}
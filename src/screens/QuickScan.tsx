import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Linking
} from 'react-native';
import {
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Text,
  Button,
  Icon,
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import QRCodeScanner from 'react-native-qrcode-scanner';
import ListBlock from './../coomponents/ListBlock';
import styles from 'IfulifeManage/src/constants/styles';

export default class QuickScan extends Component<Props>{
  qrcode: QRCodeScanner | null;

  constructor(props) {
    super(props);

    this.state = {
      decodes : {
        "activations" : [],
        "textstrings": [],
        "weburls": [],
      },
    }

    this.qrcode = null;
  }


  onSuccess = (e) => {
    console.log('擷取的資料: ', e.data);

    // 判斷是否為住戶開通碼、網址、單純字串(包裹)

    // Linking
    // .openURL(e.data)
    // .catch(err => console.error('An error occured', err));
  }


  render() {
    const { isVisible } = this.props;

    return (
      <View style={styles.modalView}>
        <View style={styles.modalViewInside}>

          <Button iconLeft light small style={styles.modalCloseBtn}
            onPress={() => { this.props.navigation.goBack(); }}
          >
            <Icon type="FontAwesome" name="window-close" style={styles.modalCloseIcon} />
            <Text style={styles.modalCloseText}>關閉</Text>
          </Button>

          <Grid style={styles.modalContent}>
            <Col style={{}}>
              <Row size={4} style={{flexDirection: 'column'}}>
                <QRCodeScanner
                  ref={(node) => { this.qrcode = node }}
                  reactivate={true}
                  reactivateTimeout={2000}
                  showMarker={true}
                  containerStyle={[styles.cameraBlock, { margin: 20, }]}
                  cameraStyle={{ width: 360, height: 360, alignSelf: 'center'}}
                  cameraType="back"
                  onRead={this.onSuccess}
                />
              </Row>
              <Row size={1} style={styles.cameraOptBlock}>
                {/* 不需按鈕直接辨識後出現toast資訊 */}

                {/* <Button block success style={styles.OptButton}>
                  <Text style={styles.OptButtonLabel}>繼續掃</Text>
                </Button>
                <Button block primary style={styles.OptButton}>
                  <Text style={styles.OptButtonLabel}>儲存關閉</Text>
                </Button> */}
              </Row>
            </Col>
            <Col style={{}}>
              <Content padder>
                {this.state.decodes["activations"].length == 0 &&
                  <ListBlock.ListItemScanActivation />
                }

                {this.state.decodes["textstrings"].length == 0 &&
                  <ListBlock.ListItemScanMpgcode />
                }

                {this.state.decodes["weburls"].length == 0 &&
                  <ListBlock.ListItemScanUrl />
                }
                <View style={{ height: 20, width: '100%' }} />
              </Content>
              <View style={{height:40, width:'100%'}} />
            </Col>
          </Grid>

        </View>
      </View>
    )
  }
}
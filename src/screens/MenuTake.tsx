import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
} from 'react-native';
import {
  Content,
  Text,
  Button,
  Icon,
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { SketchCanvas } from '@gigasz/react-native-sketch-canvas';
import SearchForm from './../coomponents/SearchForm';
import ListBlock from './../coomponents/ListBlock';
import styles from 'IfulifeManage/src/constants/styles';

export default class MenuTake extends Component<Props>{
  qrcode: QRCodeScanner | null;

  constructor(props) {
    super(props);

    this.state = {
      unitmpgs : [],
    }
  }


  render() {

    return (
      <View style={styles.modalView}>
        <View style={styles.modalViewInside}>

          <Button iconLeft light small style={styles.modalCloseBtn}
            onPress={() => { this.props.navigation.goBack(); }}
          >
            <Icon type="FontAwesome" name="window-close" style={styles.modalCloseIcon} />
            <Text style={styles.modalCloseText}>關閉</Text>
          </Button>

          <Grid style={styles.modalContent}>
            <Col style={{}}>
              <Row>
                {/* 不需按鈕直接辨識後出現dialog資訊 */}
                <SearchForm />
                <Text>搜尋住戶:輸入[姓名、戶別]</Text>
                {/* Form搜尋Bar出現列表項目點選姓名後才載入右邊內容 */}
              </Row>
              <Row>
                <QRCodeScanner
                  ref={(node) => { this.qrcode = node }}
                  reactivate={true}
                  reactivateTimeout={2000}
                  showMarker={true}
                  containerStyle={[styles.cameraBlock, { margin: 20, }]}
                  cameraStyle={{ width: 360, height: 360, alignSelf: 'center' }}
                  cameraType="back"
                  onRead={this.onSuccess}
                />
              </Row>
            </Col>
            <Col style={{}}>
              <Content padder>
                <Text>顯示待領列表資訊，選擇單筆或多筆，前景視窗確認</Text>
                <Text>確認領件品項無誤直接送出完成領件。(手寫簽收、後端丟住戶App專屬領件碼掃描)</Text>

                {this.state.unitmpgs.length == 0 &&
                  <ListBlock.ListItemTakeMpgs />
                }

                <View style={{ height: 20, width: '100%' }} />
              </Content>
              <View style={{ height: 40, width: '100%' }} />
            </Col>
          </Grid>

        </View>
      </View>
    )
  }
}
/**
 * 登入畫面
 */

import React, { Component, Fragment } from 'react';
import {
  SafeAreaView,
  StatusBar,
  Text,
  ImageBackground,
  View
} from 'react-native';
import {
  Thumbnail,
  H1,
  Item,
  Input,
  Button,
  Icon,
} from 'native-base';
import styles from 'IfulifeManage/src/constants/styles';

export default class SigninScreen extends Component<Props>{

  render(){
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <ImageBackground
          source={require('IfulifeManage/src/assets/BackgroundSignin.png')}
          style={styles.mainbackground}>
          <SafeAreaView style={styles.container}>
            <View>
              <Thumbnail square large source={require('IfulifeManage/src/assets/Logo.png')} />
              <H1>愛富生活鼎藏帝景社區管理</H1>
              <Item rounded style={styles.signInput}>
                <Input placeholder='輸入帳號' />
              </Item>
              <Item rounded style={styles.signInput}>
                <Input placeholder='輸入密碼' />
              </Item>
              <Button block warning style={{width:250, alignSelf: 'center', marginTop: 40,}} >
                <Text style={{ fontSize: 20, color: '#00629A'}}>登    入</Text>
              </Button>
            </View>
          </SafeAreaView>
        </ImageBackground>
      </Fragment>
    );
  }
}

/**
 * 主操作畫面
 */

import React, { Component, Fragment } from 'react';
import {
  SafeAreaView,
  StatusBar,
  ImageBackground,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  Content,
  Thumbnail,
  H1,
  H3,
  Text,
  Button,
  Icon
} from 'native-base';
import { Grid, Col } from 'react-native-easy-grid';
import Tooltip from 'react-native-walkthrough-tooltip';
import Device from 'react-native-detect-device';
import Config from "react-native-config";
import styles from 'IfulifeManage/src/constants/styles';
import UserGuide from './UserGuide';
import QuickScan from './QuickScan';
import QuickPhoto from './QuickPhoto';
import MenuGuest from './MenuGuest';
import MenuTake from './MenuTake';
import MenuReceive from './MenuReceive';


export default class MainScreen extends Component<Props>{
  constructor(props) {
    super(props);

    this.state = {
      toolTipVisible: false,
    }

  }


  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />

        <ImageBackground
          source={require('IfulifeManage/src/assets/Background.png')}
          style={styles.mainbackground}>
        <SafeAreaView style={styles.container}>
          <Content>
            <View style={styles.mainTopbar}>
              <Thumbnail square style={styles.mainLogo} source={require('IfulifeManage/src/assets/Logo.png')} />
              <H3 style={styles.mainHeader}>愛富管理-鼎藏帝景社區</H3>
              <Button iconRight small round style={styles.mainGuideBtn}>
                <Text>完成暫存包裹收件作業</Text>
                <Icon type="Ionicons" name="ios-arrow-forward" size={20} />
              </Button>
            </View>
            <View style={styles.notice}>
              <Tooltip
                animated
                isVisible={this.state.toolTipVisible}
                content={<UserGuide />}
                placement="bottom"
                onClose={() => this.setState({ toolTipVisible: false })}
              >
                <Button iconLeft transparent
                  onPress={() => {
                    this.setState({ toolTipVisible: true });
                  }}
                >
                  <Icon type="FontAwesome" name="question-circle" size={14} style={styles.noticeIcon} />
                  <Text style={styles.noticeLabel}>教我怎麼用</Text>
                </Button>
              </Tooltip>
            </View>

            <Grid style={styles.quick}>
              <Col style={[styles.quickCell, { flexDirection: 'row-reverse'}]}>
                <TouchableOpacity style={[styles.quickButton, {marginRight: 68,}]}
                    onPress={() => { this.props.navigation.navigate("Scan"); }}
                >
                  <Icon type="Ionicons" name="md-qr-scanner" style={styles.quickIcon} />
                  <Text style={styles.quickLabel}>掃碼辨識</Text>
                </TouchableOpacity>
              </Col>
              <Col style={[styles.quickCell, { flexDirection: 'row' }]}>
                <TouchableOpacity style={[styles.quickButton, { marginLeft: 68, }]}
                  onPress={() => { this.props.navigation.navigate("Photo"); }}
                >
                  <Icon type="MaterialIcons" name="photo-camera" style={styles.quickIcon} />
                  <Text style={styles.quickLabel}>拍照建檔</Text>
                </TouchableOpacity>
              </Col>
            </Grid>

            <Grid style={styles.menu}>
              <Col>
                <TouchableOpacity style={[styles.menuButton, styles.mBtnFirst]}
                  onPress={() => { this.props.navigation.navigate("Guest"); }}
                >
                  <Icon type="MaterialCommunityIcons" name="human-greeting" style={styles.menuIcon} />
                  <H1 style={styles.menuTitle}>訪客登記</H1>
                  <Text style={styles.menuLabel}>請簽名/輸入姓名並交換證件</Text>
                </TouchableOpacity>
              </Col>
              <Col>
                <TouchableOpacity style={[styles.menuButton, styles.mBtn]}
                  onPress={() => { this.props.navigation.navigate("Take"); }}
                >
                  <Icon type="MaterialCommunityIcons" name="package-variant" style={styles.menuIcon} />
                  <H1 style={styles.menuTitle}>包裹領件</H1>
                  <Text style={styles.menuLabel}>住戶取走包裹與郵件掛號</Text>
                </TouchableOpacity>
              </Col>
              <Col>
                <TouchableOpacity style={[styles.menuButton, styles.mBtnLast]}
                  onPress={() => { this.props.navigation.navigate("Receive"); }}
                >
                  <Icon type="MaterialCommunityIcons" name="package-variant-closed" style={styles.menuIcon} />
                  <H1 style={styles.menuTitle}>包裹收件</H1>
                  <Text style={styles.menuLabel}>從物流快遞接收包裹郵件</Text>
                </TouchableOpacity>
              </Col>
            </Grid>

          </Content>
        </SafeAreaView>
        </ImageBackground>
      </Fragment>
    );
  }
}

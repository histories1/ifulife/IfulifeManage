import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Alert
} from 'react-native';
import {
  Text,
  Form,
  Picker,
  Item,
  Input,
  Button,
  Icon,
} from 'native-base';
import { Formik } from 'formik';
import styles from 'IfulifeManage/src/constants/styles';

export default class GuestForm extends Component<Props>{

  constructor(props) {
    super(props)

    this.state = {
      logisticsItems: undefined,
      logisticsSelected: 0,
      logisticsPhone: '',
      logisticsName: '',
      logisticsCode: '',
      hasPhoto: false,
      mpgPhotos: [],
      mpgType: '',
      mpgLocation: '',
      mpgLocationSelected: "",
      mpgTypeSelected: "",
      onSaved: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onLogisticsChange = this.onLogisticsChange.bind(this);
    this.takePhoto = this.takePhoto.bind(this);
    this.onMpgTypeChange = this.onMpgTypeChange.bind(this);
    this.onMpgLocationChange = this.onMpgLocationChange.bind(this);
  };


  componentDidMount() {
  }


  handleSubmit(value) {
    console.log(value);
  }


  onLogisticsChange(logisticsSelected) {
    this.setState({ logisticsSelected });
  }



  takePhoto(logisticsSelected) {
    this.setState({ logisticsSelected });
  }


  onMpgTypeChange(mpgTypeSelected) {
    this.setState({ mpgTypeSelected });
  }


  onMpgLocationChange(mpgLocationSelected) {
    this.setState({ mpgLocationSelected });
  }


  handleSave() {
    this.setState({
      onSaved: true
    });
  }

  // 處理連結產出社區編號並印出標籤
  handleFinish() {

  }


  render() {
    return (
      <Formik
        initialValues={{ email: '' }}
        onSubmit={values => this.handleSubmit(values) }
      >
        <Form>
        <ScrollView style={styles.receiveFormBlock}>

          <Text>物流廠商</Text>
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={<Icon type="Entypo" name="arrow-down" />}
              style={{ width: undefined }}
              placeholder="選擇物流業者"
              placeholderStyle={{ color: "#CECECE" }}
              placeholderIconColor="#CECECE"
              selectedValue={this.state.logisticsSelected}
              onValueChange={(select) => this.onLogisticsChange(select)}
            >
              <Picker.Item label="中華郵政" value="0" />
              <Picker.Item label="黑貓" value="1" />
            </Picker>
          </Item>
          <Text />

          <Text>物流電話</Text>
          <Item>
            <Input placeholder="點此輸入電話號碼"
              keyboardType='number-pad'
              value={this.state.logisticsPhone}
              onChangeText={logisticsPhone => this.setState({logisticsPhone})}
            />
          </Item>
          <Text />

          <Text>物流姓名</Text>
          <Item>
            <Input placeholder="可使用''手寫輸入''選字填寫"
              keyboardType='ascii-capable'
              value={this.state.logisticsName}
              onChangeText={logisticsName => this.setState({ logisticsName })}
            />
          </Item>
          <Text />

          <Text>物流單編號</Text>
          <Item>
            <Input
              ref="qrcode"
              placeholder="點此手動輸入或右側物流單掃碼輸入"
              value={this.state.logisticsCode}
              onTouchEndCapture={() => this.props.switchCamera(1)}
            />
          </Item>
          <Text />

          <Text>包裹識別照片</Text>
          { (!this.state.hasPhoto) ?
           (
            <Item>
              <Input
                disabled
                placeholder="點選進行拍照，最多2張照片。"
                onTouchEndCapture={() => this.props.switchCamera(0)}
              />
            </Item>
           ) :
           (
            <View style={{ width:'100%', height:160, backgroundColor: '#CECECE' }}>
            {
            }
            </View>
           )
          }
          <Text />

          <Text>包裹類型</Text>
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={<Icon type="Entypo" name="arrow-down" />}
              placeholder="選擇類型"
              placeholderStyle={{ color: "#CECECE" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.mpgTypeSelected}
              onValueChange={(mpgTypeSelected) => this.onMpgTypeChange(mpgTypeSelected)}
            >
              <Picker.Item label="包裹" value="key0" />
              <Picker.Item label="掛號" value="key1" />
            </Picker>
          </Item>
          <Text />

          <Text>存放位置</Text>
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={<Icon type="Entypo" name="arrow-down" />}
              placeholder="包裹存放位置"
              placeholderStyle={{ color: "#CECECE" }}
              placeholderIconColor="#007aff"
              selectedValue={this.state.mpgLocationSelected}
              onValueChange={(mpgLocationSelected) => this.onMpgLocationChange(mpgLocationSelected)}
            >
              <Picker.Item label="櫃檯" value="key0" />
              <Picker.Item label="管理室" value="key1" />
            </Picker>
          </Item>
          <Text />
        </ScrollView>

          <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
            {/* 配置社區編號、顯示紀錄收件時間 */}
            <Button success onPress={() => this.handleSave()}>
              <Text>編輯儲存</Text>
            </Button>

            {/* 觸發連結標籤機出標籤2張 */}
            {this.state.onSaved == true &&
              <Button success onPress={() => this.handleFinish()}>
                <Text>完成取號</Text>
              </Button>
            }
          </View>
      </Form>
      </Formik>
    )
  }
}
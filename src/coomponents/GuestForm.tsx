import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Alert
} from 'react-native';
import {
  Text,
  Form,
  Picker,
  Item,
  Input,
  Button,
  Icon,
} from 'native-base';
import { Formik } from 'formik';
import styles from 'IfulifeManage/src/constants/styles';

export default class GuestForm extends Component<Props>{

  constructor(props) {
    super(props)

    this.state = {
      guestName: '',
      guestPhone: '',
      guestReason: '',
      guestReasonToUnit: '',
      guestReasonTextarea: '',
      guestPhoto: undefined,
      hasSigned: false,
      guestCanvas: undefined,
      onSaved: false
    };

    this.onReasonChange = this.onReasonChange.bind(this);
    this.onReasonToUnitChange = this.onReasonToUnitChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
  };

  onReasonChange(guestReason) {
    this.setState({ guestReason });
  }

  onReasonToUnitChange(guestReasonToUnit) {
    this.setState({ guestReasonToUnit });
  }

  onReasonTextareaChange(guestReasonTextarea) {
    this.setState({ guestReasonTextarea });
  }


  handleSave() {
    {/* 配置社區編號、顯示紀錄收件時間 */ }

    this.setState({
      guestName: '',
      guestPhone: '',
      onSaved: true
    });

    // 顯示dialog讓警衛設定訪客到訪戶別
  }

  render() {
    return (
      <Formik
        initialValues={{ email: '' }}
        onSubmit={values => console.log(values)}
      >
        <Form>
          <ScrollView style={styles.receiveFormBlock}>

            <Text>訪客姓名</Text>
            <Item>
              <Input placeholder="可使用''手寫輸入''選字填寫"
                keyboardType='ascii-capable'
                value={this.state.guestName}
                onChangeText={guestName => this.setState({ guestName })}
              />
            </Item>
            <Text />

            <Text>聯繫電話</Text>
            <Item>
              <Input placeholder="點此輸入電話號碼"
                keyboardType='number-pad'
                value={this.state.guestPhone}
                onChangeText={guestPhone => this.setState({ guestPhone })}
              />
            </Item>
            <Text />

            <Text>到訪事由</Text>
            <Item picker>
              <Picker
                mode="dialog"
                iosIcon={<Icon type="Entypo" name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="選擇到訪事由"
                placeholderStyle={{ color: "#CECECE" }}
                placeholderIconColor="#CECECE"
                selectedValue={this.state.guestReason}
                onValueChange={(select) => this.onReasonChange(select)}
              >
                <Picker.Item label="住戶訪客" value="0" />
                <Picker.Item label="社區維護廠商" value="1" />
              </Picker>
            </Item>
            {this.state.guestReason=="0" &&
              <Picker
                mode="dialog"
                iosIcon={<Icon type="Entypo" name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="社區住戶"
                placeholderStyle={{ color: "#CECECE" }}
                placeholderIconColor="#CECECE"
                selectedValue={this.state.guestReasonToUnit}
                onValueChange={(select) => this.onReasonToUnitChange(select)}
              >
                <Picker.Item label="A1-116-2F" value="0" />
                <Picker.Item label="A1-116-3F" value="1" />
                <Picker.Item label="A1-117-4F" value="2" />
              </Picker>
            }
            <Item>
              <Input
                ref="textarea"
                placeholder="選擇其他時請填寫此欄位"
                value={this.state.guestReasonTextarea}
                onChangeText={(value)=>this.onReasonTextareaChange(value)}
                onTouchEndCapture={() => this.refs.textarea.placeholder=''}
              />
            </Item>
            <Text />

          </ScrollView>

          <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
            <Button success onPress={() => this.handleSave()}>
              <Text>確認儲存</Text>
            </Button>
          </View>
        </Form>
      </Formik>
    )
  }

}
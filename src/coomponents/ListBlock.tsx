import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Linking
} from 'react-native';
import {
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Text,
  Button,
  Icon,
} from 'native-base';
import styles from 'IfulifeManage/src/constants/styles';

const ListItemScanActivation = () => {
  return (
    <List>
      <ListItem itemHeader icon>
        <Left style={{ width: '10%' }}>
          <Icon type="Entypo" name="add-user" style={{ fontSize: 24, padding: 0, margin: 0, color: 'orange' }} />
        </Left>
        <Body style={{ width: '90%' }}>
          <Text>住戶開通碼</Text>
        </Body>
      </ListItem>
      <ListItem icon first onPress={() => { alert('click!'); }}>
        <Body>
          <Text>1235353262</Text>
        </Body>
        <Right>
          <Text>動作</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
      <ListItem icon onPress={() => { alert('click!'); }}>
        <Body>
          <Text>1235353262</Text>
        </Body>
        <Right>
          <Text>動作</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
    </List>
  )
}

const ListItemScanMpgcode = () => {
  return (
    <List>
      <ListItem itemHeader icon>
        <Left style={{ width: '10%' }}>
          <Icon type="FontAwesome" name="qrcode" style={{ fontSize: 24, padding: 0, margin: 0, color: 'orange' }} />
        </Left>
        <Body style={{ width: '90%' }}>
          <Text>包裹條碼（需與住戶識別碼區分,識別碼專屬前綴）</Text>
        </Body>
      </ListItem>
      <ListItem icon first onPress={() => { alert('click!'); }}>
        <Body>
          <Text>A32141Efb</Text>
        </Body>
        <Right>
          <Text>包裹建檔</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
      <ListItem icon onPress={() => { alert('click!'); }}>
        <Body>
          <Text>A32141Efb</Text>
        </Body>
        <Right>
          <Text>包裹建檔</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
    </List>
  )
}


const ListItemScanUrl = () => {
  return (
    <List>
      <ListItem itemHeader icon>
        <Left style={{ width: '10%' }}>
          <Icon type="FontAwesome" name="external-link" style={{ fontSize: 24, padding: 0, margin: 0, color: 'orange' }} />
        </Left>
        <Body style={{ width: '90%' }}>
          <Text>連結網址</Text>
        </Body>
      </ListItem>
      <ListItem icon first onPress={() => { alert('click!'); }}>
        <Body style={{ width: '70%' }}>
          <Text numberOfLines={1} style={{ flexWrap: 'nowrap' }}>https://www.google.com.tw/</Text>
        </Body>
        <Right style={{ width: '15%' }}>
          <Text>預覽</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
    </List>
  )
}


const ListItemTakeMpgs = () => {
  return (
    <List>
      <ListItem itemHeader icon>
        <Left style={{ width: '10%' }}>
          <Icon type="FontAwesome" name="qrcode" style={{ fontSize: 24, padding: 0, margin: 0, color: 'orange' }} />
        </Left>
        <Body style={{ width: '90%' }}>
          <Text>ＸＸＸ戶別包裹清單</Text>
        </Body>
      </ListItem>
      <ListItem icon first onPress={() => { alert('click!'); }}>
        <Body>
          <Text>A32141Efb</Text>
        </Body>
        <Right>
          <Text>包裹建檔</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
      <ListItem icon onPress={() => { alert('click!'); }}>
        <Body>
          <Text>A32141Efb</Text>
        </Body>
        <Right>
          <Text>包裹建檔</Text>
          <Icon active name="arrow-forward" />
        </Right>
      </ListItem>
    </List>
  )
}

export default {
  ListItemScanActivation,
  ListItemScanMpgcode,
  ListItemScanUrl,
  ListItemTakeMpgs
}
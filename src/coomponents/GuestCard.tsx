import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Linking
} from 'react-native';
import {
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Text,
  Button,
  Icon,
} from 'native-base';
import styles from 'IfulifeManage/src/constants/styles';

export default class GuestCard extends Component<Props>{

  constructor(props) {
    super(props);

    this.state = {
      cards: [1,2,3,4]
    };
  }

  render() {
    {/* https://github.com/ocetnik/react-native-background-timer */ }
    {/* 觸發處理更新訪客事件、包裹收發事件本地端暫存、未使用照片檔清除更新 */ }
    return (
      <Content padder>
        {this.state.cards &&
          this.state.cards.map(function(item, key) {
            return (
            <Card key={key}>
              <CardItem header>
                <Text>訪客:XXX</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Text>簽入時間：2019/08/05 14:33:33</Text>
                  <Text>事由：XXX</Text>
                </Body>
              </CardItem>
              <CardItem footer>
                <Button transparent primary
                  onPress={()=>{
                    alert('設定已離開狀態，將訪客列入今日到訪！');
                  }}
                >
                  <Text>確認離開</Text>
                </Button>
              </CardItem>
            </Card>
            )
          })
        }
      </Content>
    )
  }
}

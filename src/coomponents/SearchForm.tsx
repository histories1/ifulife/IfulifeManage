// Formik x React Native example
import React from 'react';
import {
  TextInput,
  View
} from 'react-native';
import {
  Container,
  Content,
  Text,
  Body,
  Button,
  Icon,
} from 'native-base';
import { Formik } from 'formik';

const SearchForm = props => (
  <Formik
    initialValues={{ search: '' }}
    onSubmit={values => console.log(values)}
  >

    {props => (
      <Body>
        <TextInput
          onChangeText={props.handleChange('search')}
          onBlur={props.handleBlur('search')}
          value={props.values.search}
        />
        <Button small rounded onPress={props.handleSubmit} >
          <Text>Submit</Text>
        </Button>
      </Body>
    )}

  </Formik>
);
export default SearchForm;
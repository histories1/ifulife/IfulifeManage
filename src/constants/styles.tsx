import EStyleSheet from 'react-native-extended-stylesheet';
import Device from 'react-native-detect-device';
const { pixelDensity, width, height, isIos, isAndroid, isPhone, isTablet, isIphoneX } = Device;

// local variable
EStyleSheet.build({
  // 深藍色
  $mainColor: '#00629A',
  // 白色
  $subColor: '#FFFFFF',
  // 黃色
  $topbarColor: '#FAC000',
  // 定義預設字體尺寸
  $rem: width > 768 ? 24 : 18
});

// 根據指定平台配置不同樣式作法
// ...Platform.select({
//   ios: {
//   },
//   android: {
//   }
// })

const styles = EStyleSheet.create({
  center: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  mainbackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: Device.width,
    height: Device.height
  },

  signInput: {
    borderRadius: 10,
    marginHorizontal: 10,
    marginVertical: 15,
    backgroundColor: 'white'
  },

  mainTopbar: {
    marginTop:50,
    width:800,
    height: 40,
    alignSelf: 'center',
    backgroundColor: '#FAC000',
    borderRadius: 30,
    overflow: 'visible',
    flexDirection: 'row',
  },
  mainLogo: {
    position: "absolute",
    top: -7,
    left:-3
  },
  mainHeader: {
    marginLeft: 60,
    marginTop: 10,
    color: '#00629A' ,
    fontWeight: 'bold',
  },
  mainGuideBtn: {
    position: "absolute",
    borderRadius: 35,
    backgroundColor: '#00629A',
    top:6,
    right:10
  },
  notice: {
    marginTop: 10,
    flexDirection: 'row-reverse',
    width: 800,
    height: 30,
    alignSelf: 'center',
    alignItems: 'center'
  },
  noticeIcon: {
    color: '#FFFFFF',
  },
  noticeLabel: {
    color: '#FFFFFF',
    paddingLeft: 5,
    fontSize: '0.7rem'
  },
  quick: {
    width: 900,
    height: 200,
    // backgroundColor: 'yellow',
    alignSelf: 'center',
  },
  quickCell: {
    width:500,
    height:200,
    alignItems: 'center',
  },
  quickButton: {
    width: 160,
    height: 160,
    backgroundColor: 'white',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  quickIcon: {
    color: '#00629A',
    fontSize: '3rem',
  },
  quickLabel: {
    fontSize: '1rem',
  },
  menu: {
    width: 900,
    height: 350,
    // backgroundColor: 'green'
  },
  menuButton: {
    marginTop:13,
    borderRadius: 15,
    width: 268,
    height: 337,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuIcon: {
    color: '#00629A',
    fontSize: '6rem',
  },
  menuTitle: {
    color:'#00629A',
    fontWeight: 'bold',
  },
  menuLabel: {
    marginTop: 10,
    fontSize: '0.8rem',
  },
  mBtnFirst: {
    marginLeft: 18,
    marginRight: 15
  },
  mBtn: {
    marginLeft: 15,
    marginRight: 15
  },
  mBtnLast: {
    marginRight: 18,
    marginLeft: 15
  },

  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 768,
    height: 640
  },
  modalView: {
    width: '100%',
    height: '100%',
    backgroundColor: '#00000070'
  },
  modalViewInside: {
    backgroundColor: 'white',
    margin: 30,
    borderRadius: 30,
    width: Device.width-60,
    height: Device.height-60
  },
  modalCloseBtn: {
    position: "absolute",
    top: 3,
    right: 3,
    height:36,
    backgroundColor: '$topbarColor',
    zIndex: 10
  },
  modalCloseIcon: {
    fontSize: '1.2rem',
    color: '$mainColor'
  },
  modalCloseText: {
    fontSize: '0.9rem',
    color: '$mainColor'
  },
  modalContent: {
    width:'94%',
    marginHorizontal: '3%',
    height: '90%',
    marginTop: '4%',
    // borderWidth:1,
  },

  photoCameraBtn: {
    width: 400,
    height: 400,
    flexDirection: 'column',
  },
  photoCameraIcon: {
    fontSize: 200
  },

  cameraBlock: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    width: 400,
    height: 360,
  },
  cameraOptBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  OptButton: {
    marginHorizontal: 10,
    minWidth: 100,
    height: 60,
  },
  OptButtonLabel: {
    color: 'white',
    fontSize: '1.5rem',
  },
  OptButtonIcon: {
    color: 'white',
    fontSize: '1.5rem',
  },
  OptButtonSM: {
    marginHorizontal: 10,
    minWidth: 40,
    height: 30,
    marginVertical: 8,
  },
  OptButtonSMLabel: {
    color: 'white',
    fontSize: '0.7rem'
  },
  OptButtonSMIcon: {
    color: 'white',
  },
  cameraFlashOff:{
  },
  rwdIcon: {
    position: "absolute",
    top: 0,
    right: 10,
    color: 'white'
  },
  photoOptBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  photoFuncBtn: {
    width:135,
    height: 90
  },
  listBody: {
    marginTop: 10,
    width: '80%',
    lineHeight: 50,
    height: 50,
  },
  listRight: {
    marginTop: 10,
    width: '20%',
    lineHeight: 50,
    height: 50,
  },

  guestCameraBlock: {
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    width: 300,
    height: 200,
  },

  receiveFormBlock: {
    borderBottomWidth: 1,
    borderBottomColor: '$mainColor',
    width: '98%',
    marginHorizontal: '1%',
    marginBottom: '1%',
    height: '87%'
  }
});
export default styles;
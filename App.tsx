/**
 * 預設選單結構
 */
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import SigninScreen from 'IfulifeManage/src/screens/Signin';
import MainScreen from 'IfulifeManage/src/screens/Main';

import QuickPhoto from 'IfulifeManage/src/screens/QuickPhoto';
import QuickScan from 'IfulifeManage/src/screens/QuickScan';
import MenuGuest from 'IfulifeManage/src/screens/MenuGuest';
import MenuReceive from 'IfulifeManage/src/screens/MenuReceive';
import MenuTake from 'IfulifeManage/src/screens/MenuTake';

const RootStack = createStackNavigator(
  {
    Main: {
      screen: MainScreen,
      navigationOptions: {
        header: null,
      }
    },
    Guest: {
      screen: MenuGuest,
      params: {
        title: '訪客登記',
        page: 'guest',
      },
    },
    Receive: {
      screen: MenuReceive,
      params: {
        title: '包裹收件',
        page: 'receive',
      },
    },
    Take: {
      screen: MenuTake,
      params: {
        title: '包裹寄件',
        page: 'take',
      },
    },
    Photo: {
      screen: QuickPhoto,
      params: {
        title: '拍照建檔',
        page: 'photo',
      },
    },
    Scan: {
      screen: QuickScan,
      params: {
        title: '掃碼辨識',
        page: 'scan',
      },
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
);

const AppNavigator = createSwitchNavigator({
  Signin: {
    screen: SigninScreen,
    navigationOptions: {
      header: null,
    }
  },
  Default: {
    screen: RootStack,
  }
},
{
  initialRouteName: 'Default'
});

export default createAppContainer(AppNavigator);